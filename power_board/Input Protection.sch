EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:D_TVS D?
U 1 1 5E3C07B7
P 800 1350
F 0 "D?" V 754 1429 50  0000 L CNN
F 1 "SMAJ33CA" V 845 1429 50  0000 L CNN
F 2 "" H 800 1350 50  0001 C CNN
F 3 "~" H 800 1350 50  0001 C CNN
	1    800  1350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E3C1AB3
P 800 1575
F 0 "#PWR?" H 800 1325 50  0001 C CNN
F 1 "GND" H 805 1402 50  0000 C CNN
F 2 "" H 800 1575 50  0001 C CNN
F 3 "" H 800 1575 50  0001 C CNN
	1    800  1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  1575 800  1525
$Comp
L Device:R_Small_US R?
U 1 1 5E438FC2
P 1425 4300
F 0 "R?" H 1493 4346 50  0000 L CNN
F 1 "660" H 1493 4255 50  0000 L CNN
F 2 "" H 1425 4300 50  0001 C CNN
F 3 "~" H 1425 4300 50  0001 C CNN
	1    1425 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1425 4075 1425 4200
Wire Wire Line
	1425 4400 1425 4425
Wire Wire Line
	1425 4425 1925 4425
Connection ~ 1425 4425
Wire Wire Line
	1425 4425 1425 4450
Text Label 2200 4600 0    50   ~ 0
3V3_RECT
Text HLabel 675  1050 0    50   Input ~ 0
Vin
Wire Wire Line
	1150 6150 1375 6150
Wire Wire Line
	4125 5125 4125 6050
Wire Wire Line
	4125 6050 4375 6050
Wire Wire Line
	2400 4925 2400 4800
Text Label 2400 4925 0    50   ~ 0
GND_RECT
Wire Wire Line
	2200 5525 2200 5425
Text Label 2200 5525 0    50   ~ 0
GND_RECT
Wire Wire Line
	1475 5225 1625 5225
Wire Wire Line
	1475 5550 1475 5225
Text Label 1475 5550 0    50   ~ 0
GND_RECT
Wire Wire Line
	2400 4600 2200 4600
$Comp
L Device:C_Small C?
U 1 1 5E46FB8E
P 2400 4700
F 0 "C?" H 2492 4746 50  0000 L CNN
F 1 "100n" H 2492 4655 50  0000 L CNN
F 2 "" H 2400 4700 50  0001 C CNN
F 3 "~" H 2400 4700 50  0001 C CNN
	1    2400 4700
	1    0    0    -1  
$EndComp
Connection ~ 1375 6150
Wire Wire Line
	1375 5025 1625 5025
Wire Wire Line
	1375 6150 1375 5025
Wire Wire Line
	2700 5675 1900 5675
Wire Wire Line
	2900 5675 2975 5675
$Comp
L Device:R_Small_US R?
U 1 1 5E45B2AD
P 2800 5675
F 0 "R?" V 2595 5675 50  0000 C CNN
F 1 "330" V 2686 5675 50  0000 C CNN
F 2 "" H 2800 5675 50  0001 C CNN
F 3 "~" H 2800 5675 50  0001 C CNN
	1    2800 5675
	0    1    1    0   
$EndComp
Connection ~ 2975 5125
Wire Wire Line
	2975 5125 2600 5125
Wire Wire Line
	1900 5225 2000 5225
Connection ~ 1900 5225
Wire Wire Line
	2975 5675 2975 5125
Wire Wire Line
	1900 5225 1900 5675
Wire Wire Line
	1925 5025 2000 5025
Connection ~ 1925 5025
Wire Wire Line
	1925 4425 1925 5025
Wire Wire Line
	1825 5225 1900 5225
$Comp
L Device:R_Small_US R?
U 1 1 5E44EB19
P 1725 5225
F 0 "R?" V 1625 5225 50  0000 C CNN
F 1 "10k" V 1550 5225 50  0000 C CNN
F 2 "" H 1725 5225 50  0001 C CNN
F 3 "~" H 1725 5225 50  0001 C CNN
	1    1725 5225
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1825 5025 1925 5025
Wire Wire Line
	1425 4725 1425 4650
Text Label 1425 4725 0    50   ~ 0
GND_RECT
$Comp
L Device:R_Small_US R?
U 1 1 5E43832F
P 1425 4550
F 0 "R?" H 1357 4504 50  0000 R CNN
F 1 "660" H 1357 4595 50  0000 R CNN
F 2 "" H 1425 4550 50  0001 C CNN
F 3 "~" H 1425 4550 50  0001 C CNN
	1    1425 4550
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LMV321 U?
U 1 1 5E432D77
P 2300 5125
F 0 "U?" H 2644 5171 50  0000 L CNN
F 1 "LMV321" H 2644 5080 50  0000 L CNN
F 2 "" H 2300 5125 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmv324.pdf" H 2300 5125 50  0001 C CNN
	1    2300 5125
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5E432592
P 1725 5025
F 0 "R?" V 1520 5025 50  0000 C CNN
F 1 "10k" V 1611 5025 50  0000 C CNN
F 2 "" H 1725 5025 50  0001 C CNN
F 3 "~" H 1725 5025 50  0001 C CNN
	1    1725 5025
	0    1    1    0   
$EndComp
Text Label 3475 6450 0    50   ~ 0
3V3_RECT
Wire Wire Line
	6350 6275 6350 6200
Wire Wire Line
	6425 6275 6350 6275
Text Label 6425 6275 0    50   ~ 0
GND_RECT
Text Label 1000 7125 0    50   ~ 0
GND_RECT
$Comp
L Device:C_Small C?
U 1 1 5E41A0EE
P 6350 6100
F 0 "C?" H 6442 6146 50  0000 L CNN
F 1 "100n" H 6442 6055 50  0000 L CNN
F 2 "" H 6350 6100 50  0001 C CNN
F 3 "~" H 6350 6100 50  0001 C CNN
	1    6350 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 7000 4125 7000
Connection ~ 3800 7000
Wire Wire Line
	3800 6450 3800 7000
Connection ~ 4125 7000
Wire Wire Line
	4375 7000 4125 7000
Wire Wire Line
	4375 7100 4375 7000
Wire Wire Line
	3800 7200 4125 7200
Connection ~ 4125 7200
Connection ~ 3800 7200
$Comp
L Device:C_Small C?
U 1 1 5E411BA0
P 4125 7100
F 0 "C?" H 4217 7146 50  0000 L CNN
F 1 "4.7u" H 4217 7055 50  0000 L CNN
F 2 "" H 4125 7100 50  0001 C CNN
F 3 "~" H 4125 7100 50  0001 C CNN
	1    4125 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E4119B8
P 3800 7100
F 0 "C?" H 3892 7146 50  0000 L CNN
F 1 "100n" H 3892 7055 50  0000 L CNN
F 2 "" H 3800 7100 50  0001 C CNN
F 3 "~" H 3800 7100 50  0001 C CNN
	1    3800 7100
	1    0    0    -1  
$EndComp
Connection ~ 2925 7125
Wire Wire Line
	2925 7200 2925 7125
Wire Wire Line
	4375 7200 4125 7200
$Comp
L power:GND #PWR?
U 1 1 5E3C9092
P 1375 6775
F 0 "#PWR?" H 1375 6525 50  0001 C CNN
F 1 "GND" H 1380 6602 50  0000 C CNN
F 2 "" H 1375 6775 50  0001 C CNN
F 3 "" H 1375 6775 50  0001 C CNN
	1    1375 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 6775 1375 6750
Wire Wire Line
	1000 6450 1000 7125
Wire Wire Line
	1000 7125 2025 7125
Wire Wire Line
	2925 6625 2925 6450
Wire Wire Line
	2925 6850 2925 6875
Connection ~ 2925 6850
Wire Wire Line
	2550 6850 2925 6850
Wire Wire Line
	2925 6825 2925 6850
Wire Wire Line
	2900 6450 2925 6450
Text Notes 1900 6350 0    50   ~ 0
5-40v
Text Notes 2950 6450 0    50   ~ 0
3v3 500mA
Wire Wire Line
	2925 7125 2925 7075
$Comp
L Device:R_Small_US R?
U 1 1 5E3ED534
P 2925 6975
F 0 "R?" H 2993 7021 50  0000 L CNN
F 1 "3.65k" H 2993 6930 50  0000 L CNN
F 2 "" H 2925 6975 50  0001 C CNN
F 3 "~" H 2925 6975 50  0001 C CNN
	1    2925 6975
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5E3ECFBF
P 2925 6725
F 0 "R?" H 2993 6771 50  0000 L CNN
F 1 "6.04k" H 2993 6680 50  0000 L CNN
F 2 "" H 2925 6725 50  0001 C CNN
F 3 "~" H 2925 6725 50  0001 C CNN
	1    2925 6725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 6450 2025 6450
$Comp
L copperforge:LM317M_STM U?
U 1 1 5E3E91F6
P 2550 6250
F 0 "U?" H 2550 6315 50  0000 C CNN
F 1 "LM317M_STM" H 2550 6224 50  0000 C CNN
F 2 "" H 2550 6250 50  0001 C CNN
F 3 "" H 2550 6250 50  0001 C CNN
	1    2550 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 6450 1075 6450
$Comp
L copperforge:STM32G030F U?
U 1 1 5E3CECFB
P 5175 5650
F 0 "U?" H 5250 5615 50  0000 C CNN
F 1 "STM32G030F" H 5250 5524 50  0000 C CNN
F 2 "" H 5775 5500 50  0001 C CNN
F 3 "" H 5775 5500 50  0001 C CNN
	1    5175 5650
	-1   0    0    -1  
$EndComp
$Comp
L Diode_Bridge:MB2S D?
U 1 1 5E3BC286
P 1375 6450
F 0 "D?" H 1719 6496 50  0000 L CNN
F 1 "MB2S" H 1719 6405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-269AA" H 1525 6575 50  0001 L CNN
F 3 "http://www.vishay.com/docs/88661/mb2s.pdf" H 1375 6450 50  0001 C CNN
	1    1375 6450
	1    0    0    -1  
$EndComp
Text HLabel 1150 6150 0    50   Input ~ 0
Vin
Wire Wire Line
	675  1050 800  1050
Wire Wire Line
	800  1050 800  1200
Connection ~ 800  1050
$Comp
L Device:C_Small C?
U 1 1 5E4DA8FD
P 1350 1200
F 0 "C?" H 1442 1246 50  0000 L CNN
F 1 "1uF" H 1442 1155 50  0000 L CNN
F 2 "" H 1350 1200 50  0001 C CNN
F 3 "~" H 1350 1200 50  0001 C CNN
	1    1350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1100 1350 1050
Wire Wire Line
	800  1050 1350 1050
Wire Wire Line
	1350 1300 1350 1525
Wire Wire Line
	1350 1525 800  1525
Connection ~ 800  1525
Wire Wire Line
	800  1525 800  1500
Text Notes 3150 5125 0    50   ~ 0
Vbus sense
Text Notes 1950 4450 0    50   ~ 0
+/- 50v -> 0-3.3v
$Comp
L Device:C_Small C?
U 1 1 5E4EB549
P 3350 6800
F 0 "C?" H 3442 6846 50  0000 L CNN
F 1 "4.7u" H 3442 6755 50  0000 L CNN
F 2 "" H 3350 6800 50  0001 C CNN
F 3 "~" H 3350 6800 50  0001 C CNN
	1    3350 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2925 6450 3350 6450
Connection ~ 2925 6450
Wire Wire Line
	2925 7200 3350 7200
$Comp
L Device:C_Small C?
U 1 1 5E4FBE2C
P 2025 6800
F 0 "C?" H 2117 6846 50  0000 L CNN
F 1 "1u" H 2117 6755 50  0000 L CNN
F 2 "" H 2025 6800 50  0001 C CNN
F 3 "~" H 2025 6800 50  0001 C CNN
	1    2025 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2025 6700 2025 6450
Connection ~ 2025 6450
Wire Wire Line
	2025 6450 2200 6450
Wire Wire Line
	2025 6900 2025 7125
Connection ~ 2025 7125
Wire Wire Line
	2025 7125 2925 7125
Wire Wire Line
	3350 6700 3350 6450
Connection ~ 3350 6450
Wire Wire Line
	3350 6450 3800 6450
Wire Wire Line
	3350 6900 3350 7200
Connection ~ 3350 7200
Wire Wire Line
	3350 7200 3800 7200
Wire Wire Line
	2975 5125 4125 5125
Wire Wire Line
	2200 4600 2200 4825
Text Label 5900 7100 0    50   ~ 0
SWCLK
Wire Wire Line
	5900 7100 5825 7100
$Sheet
S 1575 975  500  150 
U 5E5BFF39
F0 "do not open" 50
F1 "schottky.sch" 50
F2 "Vin" I L 1575 1050 50 
F3 "Vout" O R 2075 1050 50 
$EndSheet
Wire Wire Line
	1350 1050 1575 1050
Connection ~ 1350 1050
Text Notes 2075 825  0    50   ~ 0
not negative
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5E6522A2
P 3700 3425
F 0 "Q?" V 4042 3425 50  0000 C CNN
F 1 "SIRA80DP" V 3950 3375 50  0000 C CNN
F 2 "" H 3900 3525 50  0001 C CNN
F 3 "~" H 3700 3425 50  0001 C CNN
	1    3700 3425
	0    -1   -1   0   
$EndComp
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5E676AF8
P 5375 1150
F 0 "Q?" V 5717 1150 50  0000 C CNN
F 1 "AUIR3241S" V 5626 1150 50  0000 C CNN
F 2 "" H 5575 1250 50  0001 C CNN
F 3 "~" H 5375 1150 50  0001 C CNN
	1    5375 1150
	0    -1   -1   0   
$EndComp
Text HLabel 6925 1050 2    50   Output ~ 0
Vout
$Comp
L copperforge:MIC5014 U?
U 1 1 5E6B6A42
P 6425 1075
F 0 "U?" H 6450 940 50  0000 C CNN
F 1 "MIC5014" H 6450 849 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 6425 1075 50  0001 C CNN
F 3 "" H 6425 1075 50  0001 C CNN
	1    6425 1075
	1    0    0    -1  
$EndComp
$Comp
L copperforge:MIC5014 U?
U 1 1 5E6E48C6
P 4650 3250
F 0 "U?" H 4675 3115 50  0000 C CNN
F 1 "MIC5014" H 4675 3024 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 4650 3250 50  0001 C CNN
F 3 "" H 4650 3250 50  0001 C CNN
	1    4650 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5E6FF72B
P 3175 1725
F 0 "Q?" H 3380 1771 50  0000 L CNN
F 1 "SIRA80DP" H 3380 1680 50  0000 L CNN
F 2 "" H 3375 1825 50  0001 C CNN
F 3 "~" H 3175 1725 50  0001 C CNN
	1    3175 1725
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5E74E193
P 3575 1725
F 0 "R?" V 3370 1725 50  0000 C CNN
F 1 "100" V 3461 1725 50  0000 C CNN
F 2 "" H 3575 1725 50  0001 C CNN
F 3 "~" H 3575 1725 50  0001 C CNN
	1    3575 1725
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5E74E565
P 3325 2075
F 0 "R?" V 3120 2075 50  0000 C CNN
F 1 "10k" V 3211 2075 50  0000 C CNN
F 2 "" H 3325 2075 50  0001 C CNN
F 3 "~" H 3325 2075 50  0001 C CNN
	1    3325 2075
	0    1    1    0   
$EndComp
Text Label 4300 4175 0    50   ~ 0
Bank_Connect_FET
$Comp
L Device:C_Small C?
U 1 1 5E7D17A0
P 5200 3900
F 0 "C?" H 5292 3946 50  0000 L CNN
F 1 "22u" H 5292 3855 50  0000 L CNN
F 2 "" H 5200 3900 50  0001 C CNN
F 3 "~" H 5200 3900 50  0001 C CNN
	1    5200 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3800 5100 3800
Wire Wire Line
	5100 3800 5100 3850
Wire Wire Line
	5100 3850 5050 3850
Wire Wire Line
	5050 3950 5100 3950
Wire Wire Line
	5100 3950 5100 4000
Wire Wire Line
	5100 4000 5200 4000
Text Label 5200 3775 0    50   ~ 0
VBus
Wire Wire Line
	5200 3775 5200 3800
Connection ~ 5200 3800
$Comp
L power:GND #PWR?
U 1 1 5E80D806
P 5200 4025
F 0 "#PWR?" H 5200 3775 50  0001 C CNN
F 1 "GND" H 5205 3852 50  0000 C CNN
F 2 "" H 5200 4025 50  0001 C CNN
F 3 "" H 5200 4025 50  0001 C CNN
	1    5200 4025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4025 5200 4000
Connection ~ 5200 4000
Wire Wire Line
	5575 1050 5725 1050
$Comp
L Device:R_Small_US R?
U 1 1 5E81F23C
P 5375 1475
F 0 "R?" V 5170 1475 50  0000 C CNN
F 1 "100" V 5261 1475 50  0000 C CNN
F 2 "" H 5375 1475 50  0001 C CNN
F 3 "~" H 5375 1475 50  0001 C CNN
	1    5375 1475
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5E81F6D8
P 5725 1200
F 0 "R?" H 5793 1246 50  0000 L CNN
F 1 "10k" H 5793 1155 50  0000 L CNN
F 2 "" H 5725 1200 50  0001 C CNN
F 3 "~" H 5725 1200 50  0001 C CNN
	1    5725 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5375 1350 5375 1375
Wire Wire Line
	5725 1300 5725 1350
Wire Wire Line
	5725 1350 5375 1350
Connection ~ 5375 1350
Wire Wire Line
	5725 1100 5725 1050
Connection ~ 5725 1050
Wire Wire Line
	6075 1575 5375 1575
Wire Wire Line
	6075 1475 6075 1050
Text Label 6225 1975 0    50   ~ 0
Board_Power_FET
Wire Wire Line
	6225 1975 6075 1975
Wire Wire Line
	6075 1975 6075 1775
$Comp
L Device:C_Small C?
U 1 1 5E85663C
P 6950 1725
F 0 "C?" H 7042 1771 50  0000 L CNN
F 1 "22u" H 7042 1680 50  0000 L CNN
F 2 "" H 6950 1725 50  0001 C CNN
F 3 "~" H 6950 1725 50  0001 C CNN
	1    6950 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1625 6875 1625
Wire Wire Line
	6875 1625 6875 1675
Wire Wire Line
	6875 1675 6825 1675
Wire Wire Line
	6825 1775 6875 1775
Wire Wire Line
	6875 1775 6875 1825
Wire Wire Line
	6875 1825 6950 1825
Text Label 6950 1600 0    50   ~ 0
VBus
Wire Wire Line
	6950 1600 6950 1625
Connection ~ 6950 1625
$Comp
L power:GND #PWR?
U 1 1 5E85664B
P 6950 1850
F 0 "#PWR?" H 6950 1600 50  0001 C CNN
F 1 "GND" H 6955 1677 50  0000 C CNN
F 2 "" H 6950 1850 50  0001 C CNN
F 3 "" H 6950 1850 50  0001 C CNN
	1    6950 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1850 6950 1825
Connection ~ 6950 1825
Wire Wire Line
	3075 1050 3075 1525
Text Label 2575 1050 0    50   ~ 0
VBus
Wire Wire Line
	5725 1050 6075 1050
Connection ~ 6075 1050
Wire Wire Line
	6075 1050 6925 1050
Text HLabel 4300 2975 2    50   BiDi ~ 0
DCLink_bank
Wire Wire Line
	3800 2975 3750 2975
$Comp
L Device:D D?
U 1 1 5E626E63
P 3700 2650
F 0 "D?" H 3700 2866 50  0000 C CNN
F 1 "D" H 3700 2775 50  0000 C CNN
F 2 "" H 3700 2650 50  0001 C CNN
F 3 "~" H 3700 2650 50  0001 C CNN
	1    3700 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5E62657A
P 3900 2975
F 0 "R?" V 3695 2975 50  0000 C CNN
F 1 "1" V 3786 2975 50  0000 C CNN
F 2 "" H 3900 2975 50  0001 C CNN
F 3 "~" H 3900 2975 50  0001 C CNN
	1    3900 2975
	0    1    1    0   
$EndComp
$Comp
L Device:L L?
U 1 1 5E625D18
P 3600 2975
F 0 "L?" V 3790 2975 50  0000 C CNN
F 1 "47u" V 3699 2975 50  0000 C CNN
F 2 "" H 3600 2975 50  0001 C CNN
F 3 "~" H 3600 2975 50  0001 C CNN
	1    3600 2975
	0    -1   -1   0   
$EndComp
Text Label 3975 2150 0    50   ~ 0
Bank_Charge_FET
Wire Wire Line
	3975 2150 3850 2150
Connection ~ 4800 1975
Wire Wire Line
	4800 2000 4800 1975
$Comp
L power:GND #PWR?
U 1 1 5E801EBF
P 4800 2000
F 0 "#PWR?" H 4800 1750 50  0001 C CNN
F 1 "GND" H 4805 1827 50  0000 C CNN
F 2 "" H 4800 2000 50  0001 C CNN
F 3 "" H 4800 2000 50  0001 C CNN
	1    4800 2000
	1    0    0    -1  
$EndComp
Connection ~ 4800 1775
Wire Wire Line
	4800 1750 4800 1775
Text Label 4800 1750 0    50   ~ 0
VBus
Wire Wire Line
	4725 1975 4800 1975
Wire Wire Line
	4725 1925 4725 1975
Wire Wire Line
	4675 1925 4725 1925
Wire Wire Line
	4725 1825 4675 1825
Wire Wire Line
	4725 1775 4725 1825
Wire Wire Line
	4800 1775 4725 1775
$Comp
L Device:C_Small C?
U 1 1 5E774BA1
P 4800 1875
F 0 "C?" H 4892 1921 50  0000 L CNN
F 1 "22u" H 4892 1830 50  0000 L CNN
F 2 "" H 4800 1875 50  0001 C CNN
F 3 "~" H 4800 1875 50  0001 C CNN
	1    4800 1875
	1    0    0    -1  
$EndComp
$Comp
L copperforge:MIC5014 U?
U 1 1 5E73FE5A
P 4275 1225
F 0 "U?" H 4300 1090 50  0000 C CNN
F 1 "MIC5014" H 4300 999 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 4275 1225 50  0001 C CNN
F 3 "" H 4275 1225 50  0001 C CNN
	1    4275 1225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3375 1725 3425 1725
Wire Wire Line
	3425 2075 3425 1725
Connection ~ 3425 1725
Wire Wire Line
	3425 1725 3475 1725
Wire Wire Line
	3225 2075 3075 2075
Wire Wire Line
	3075 2075 3075 1925
Wire Wire Line
	3925 1725 3675 1725
Wire Wire Line
	3925 1625 3725 1625
Wire Wire Line
	3725 1625 3725 2200
Wire Wire Line
	3725 2200 3075 2200
Wire Wire Line
	3075 2200 3075 2075
Connection ~ 3075 2075
Wire Wire Line
	3850 2150 3850 1925
Wire Wire Line
	3850 1925 3925 1925
Wire Wire Line
	3550 2650 3425 2650
Wire Wire Line
	3425 2650 3425 2975
Connection ~ 3425 2975
Wire Wire Line
	3425 2975 3450 2975
Wire Wire Line
	3850 2650 4050 2650
Wire Wire Line
	4050 2650 4050 2975
Connection ~ 4050 2975
Wire Wire Line
	4050 2975 4000 2975
Wire Wire Line
	4050 2975 4150 2975
Wire Wire Line
	3075 2975 3350 2975
Wire Wire Line
	3500 3325 3350 3325
Wire Wire Line
	3350 3325 3350 2975
Connection ~ 3350 2975
Wire Wire Line
	3350 2975 3425 2975
Wire Wire Line
	3900 3325 3975 3325
Wire Wire Line
	4150 3325 4150 2975
Connection ~ 4150 2975
Wire Wire Line
	4150 2975 4300 2975
Wire Wire Line
	2075 1050 2425 1050
$Comp
L Device:R_Small_US R?
U 1 1 5EA772FC
P 3700 3725
F 0 "R?" V 3495 3725 50  0000 C CNN
F 1 "100" V 3586 3725 50  0000 C CNN
F 2 "" H 3700 3725 50  0001 C CNN
F 3 "~" H 3700 3725 50  0001 C CNN
	1    3700 3725
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5EA7789F
P 3975 3500
F 0 "R?" H 4043 3546 50  0000 L CNN
F 1 "10k" H 4043 3455 50  0000 L CNN
F 2 "" H 3975 3500 50  0001 C CNN
F 3 "~" H 3975 3500 50  0001 C CNN
	1    3975 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3625 3975 3625
Wire Wire Line
	3975 3625 3975 3600
Wire Wire Line
	3975 3400 3975 3325
Connection ~ 3975 3325
Wire Wire Line
	3975 3325 4150 3325
Wire Wire Line
	4300 3650 4300 3325
Wire Wire Line
	4300 3325 4150 3325
Connection ~ 4150 3325
Connection ~ 3700 3625
Wire Wire Line
	3700 3825 3700 3875
Wire Wire Line
	3700 3875 4100 3875
Wire Wire Line
	4100 3875 4100 3750
Wire Wire Line
	4100 3750 4300 3750
Wire Wire Line
	4300 4175 4225 4175
Wire Wire Line
	4225 4175 4225 3950
Wire Wire Line
	4225 3950 4300 3950
Text Notes 5100 725  0    50   ~ 0
0.6m rdson
Text Notes 3850 2400 0    50   ~ 0
Charge DC link bank through\nsoft start circuit
Text Notes 4250 4350 0    50   ~ 0
Override soft start circuitry
Wire Wire Line
	3075 2650 3075 2725
Wire Wire Line
	3075 2450 3075 2400
Connection ~ 3075 2200
$Comp
L Device:R_Small_US R?
U 1 1 5EAAF57F
P 3075 2550
F 0 "R?" H 3143 2596 50  0000 L CNN
F 1 "500u" H 3143 2505 50  0000 L CNN
F 2 "" H 3075 2550 50  0001 C CNN
F 3 "~" H 3075 2550 50  0001 C CNN
	1    3075 2550
	1    0    0    -1  
$EndComp
$Comp
L copperforge:INAx180 U?
U 1 1 5EACF17F
P 2350 2450
F 0 "U?" H 2425 2765 50  0000 C CNN
F 1 "INA4180" H 2425 2674 50  0000 C CNN
F 2 "" H 2600 2650 50  0001 C CNN
F 3 "" H 2600 2650 50  0001 C CNN
	1    2350 2450
	-1   0    0    -1  
$EndComp
Text Notes 2025 2025 0    50   ~ 0
200V/V
Wire Wire Line
	2600 2450 2700 2450
Text Label 1625 2600 2    50   ~ 0
3V3_RECT
Wire Wire Line
	1625 2600 1700 2600
$Comp
L power:GND #PWR?
U 1 1 5EAF512C
P 1625 2800
F 0 "#PWR?" H 1625 2550 50  0001 C CNN
F 1 "GND" H 1630 2627 50  0000 C CNN
F 2 "" H 1625 2800 50  0001 C CNN
F 3 "" H 1625 2800 50  0001 C CNN
	1    1625 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1625 2800 1700 2800
$Comp
L Device:C_Small C?
U 1 1 5EB136D7
P 1700 2700
F 0 "C?" H 1792 2746 50  0000 L CNN
F 1 "1u" H 1792 2655 50  0000 L CNN
F 2 "" H 1700 2700 50  0001 C CNN
F 3 "~" H 1700 2700 50  0001 C CNN
	1    1700 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2800 1950 2800
Wire Wire Line
	1950 2800 1950 2750
Wire Wire Line
	1950 2650 1950 2600
Wire Wire Line
	1950 2600 1700 2600
Connection ~ 1700 2600
Connection ~ 1700 2800
$Comp
L Device:R_Small_US R?
U 1 1 5EB434DF
P 2800 2400
F 0 "R?" V 2595 2400 50  0000 C CNN
F 1 "100" V 2686 2400 50  0000 C CNN
F 2 "" H 2800 2400 50  0001 C CNN
F 3 "~" H 2800 2400 50  0001 C CNN
	1    2800 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 2650 2700 2650
$Comp
L Device:R_Small_US R?
U 1 1 5EB4C2F8
P 2800 2725
F 0 "R?" V 2595 2725 50  0000 C CNN
F 1 "100" V 2686 2725 50  0000 C CNN
F 2 "" H 2800 2725 50  0001 C CNN
F 3 "~" H 2800 2725 50  0001 C CNN
	1    2800 2725
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 2450 2700 2400
Wire Wire Line
	2700 2650 2700 2725
Wire Wire Line
	2900 2725 3075 2725
Connection ~ 3075 2725
Wire Wire Line
	3075 2725 3075 2975
Wire Wire Line
	2900 2400 3075 2400
Connection ~ 3075 2400
Wire Wire Line
	3075 2400 3075 2200
Text Label 1875 2400 2    50   ~ 0
DC_Link_Current
Wire Wire Line
	1875 2400 1950 2400
Text Label 4275 6150 2    50   ~ 0
DC_Link_Current
Wire Wire Line
	4275 6150 4375 6150
Text Label 5900 6800 0    50   ~ 0
Bank_Charge_FET
Wire Wire Line
	5900 6800 5825 6800
Text Label 4325 6350 2    50   ~ 0
UART_RX
Text Label 4325 6250 2    50   ~ 0
UART_TX
Wire Wire Line
	4325 6250 4375 6250
Wire Wire Line
	4325 6350 4375 6350
Wire Wire Line
	5825 6000 6350 6000
Text Label 5900 6300 0    50   ~ 0
I2C_SCL
Text Label 5900 6450 0    50   ~ 0
I2C_SDA
Wire Wire Line
	5900 6450 5825 6450
Wire Wire Line
	5825 6300 5900 6300
Text Label 6350 6000 0    50   ~ 0
NRST
Text Label 5900 7000 0    50   ~ 0
SWDIO
Wire Wire Line
	5900 7000 5825 7000
Text Label 3700 5125 0    50   ~ 0
Vbus_sense
Text Label 1425 4075 0    50   ~ 0
3V3_RECT
Text Label 4325 6750 2    50   ~ 0
MOSI
Text Label 4325 6550 2    50   ~ 0
SCK
Wire Wire Line
	4325 6550 4375 6550
Wire Wire Line
	4325 6750 4375 6750
$Comp
L Device:R_Small_US R?
U 1 1 5ECC6D98
P 2425 1200
F 0 "R?" H 2493 1246 50  0000 L CNN
F 1 "10k" H 2493 1155 50  0000 L CNN
F 2 "" H 2425 1200 50  0001 C CNN
F 3 "~" H 2425 1200 50  0001 C CNN
	1    2425 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5ECC71C1
P 2425 1450
F 0 "R?" H 2493 1496 50  0000 L CNN
F 1 "1k" H 2493 1405 50  0000 L CNN
F 2 "" H 2425 1450 50  0001 C CNN
F 3 "~" H 2425 1450 50  0001 C CNN
	1    2425 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ECC771C
P 2425 1600
F 0 "#PWR?" H 2425 1350 50  0001 C CNN
F 1 "GND" H 2430 1427 50  0000 C CNN
F 2 "" H 2425 1600 50  0001 C CNN
F 3 "" H 2425 1600 50  0001 C CNN
	1    2425 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2425 1600 2425 1550
Wire Wire Line
	2425 1350 2425 1325
Wire Wire Line
	2425 1100 2425 1050
Wire Wire Line
	2425 1325 2675 1325
Connection ~ 2425 1325
Wire Wire Line
	2425 1325 2425 1300
Text Label 2675 1325 0    50   ~ 0
VBus_Div
Text Label 4325 6450 2    50   ~ 0
VBus_Div
Wire Wire Line
	4325 6450 4375 6450
Wire Wire Line
	2425 1050 3075 1050
Connection ~ 2425 1050
Connection ~ 3075 1050
Wire Wire Line
	3075 1050 5175 1050
$EndSCHEMATC
